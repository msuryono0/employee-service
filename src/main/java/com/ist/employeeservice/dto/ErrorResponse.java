package com.ist.employeeservice.dto;

import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Builder;
import lombok.Data;
import org.springframework.http.HttpStatus;

@Builder
@Data
@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
public class ErrorResponse {
    private HttpStatus status;
    private String message;
    private StackTraceElement[] stackTrace;

    public ErrorResponse(HttpStatus httpStatus) {
        this.status = httpStatus;
    }

    public ErrorResponse(HttpStatus httpStatus, String message) {
        this.status = httpStatus;
        this.message = message;
    }

    public ErrorResponse(HttpStatus httpStatus, String message, StackTraceElement[] stackTrace) {
        this.status = httpStatus;
        this.message = message;
        this.stackTrace = stackTrace;
    }
}
