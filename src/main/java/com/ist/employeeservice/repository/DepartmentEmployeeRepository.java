package com.ist.employeeservice.repository;

import com.ist.employeeservice.model.DepartmentEmployee;
import com.ist.employeeservice.model.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DepartmentEmployeeRepository extends JpaRepository<DepartmentEmployee, Integer> {
    DepartmentEmployee findByEmployeeId(Employee employee);
}
