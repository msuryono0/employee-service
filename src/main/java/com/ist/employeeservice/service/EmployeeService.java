package com.ist.employeeservice.service;

import com.ist.employeeservice.dto.EmployeeRequest;
import com.ist.employeeservice.dto.EmployeeUpdateRequest;
import com.ist.employeeservice.dto.GeneralResponse;
import com.ist.employeeservice.model.Department;
import com.ist.employeeservice.model.DepartmentEmployee;
import com.ist.employeeservice.model.Employee;
import com.ist.employeeservice.repository.DepartmentEmployeeRepository;
import com.ist.employeeservice.repository.DepartmentRepository;
import com.ist.employeeservice.repository.EmployeeRepository;
import org.hibernate.FetchNotFoundException;
import org.hibernate.exception.DataException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

@Service
public class EmployeeService {

    @Autowired
    EmployeeRepository employeeRepository;

    @Autowired
    DepartmentRepository departmentRepository;

    @Autowired
    DepartmentEmployeeRepository departmentEmployeeRepository;

    public GeneralResponse getAllEmployee() {
        List<Employee> employees = employeeRepository.findAll();
        return GeneralResponse.builder()
                .status(HttpStatus.OK)
                .message("Success get data employee")
                .data(employees)
                .build();
    }

    @Transactional(rollbackFor = { SQLException.class })
    public GeneralResponse  createEmployee(EmployeeRequest request) throws DataException, NullPointerException {

        Boolean existEmployee = employeeRepository.existsByNik(request.getNik());
        if (existEmployee) throw new DataException("Record already exist for NIK " + request.getNik(), null);

        Department department = this.getDepartment(request.getDepartmentId());
        if (department == null) throw new NullPointerException("Data department not found");

        Employee employee = new Employee();
        employee.setAddress(request.getAddress());
        employee.setFirstName(request.getFirstName());
        employee.setLastName(request.getLastName());
        employee.setGender(request.getGender());
        employee.setHireDate(request.getHireDate());
        employee.setPlaceOfBirth(request.getPlaceOfBirth());
        employee.setDateOfBirth(request.getDateOfBirth());
        employee.setNik(request.getNik());
        employeeRepository.save(employee);

        DepartmentEmployee departmentEmployee = new DepartmentEmployee();
        departmentEmployee.setEmployeeId(employee);
        departmentEmployee.setDepartmentId(department);
        departmentEmployeeRepository.save(departmentEmployee);

        return GeneralResponse.builder().data(employee).message("Success create").status(HttpStatus.OK).build();
    }

    @Transactional(rollbackFor = { SQLException.class, NullPointerException.class })
    public GeneralResponse update(EmployeeUpdateRequest request, Integer id) {
        System.out.println(id);
        Optional<Employee> employeeOptional = employeeRepository.findById(id);
        if (employeeOptional.isEmpty()) throw new NullPointerException("Data employee not found");

        System.out.println(employeeOptional.get());
        Department department = this.getDepartment(request.getDepartmentId());
        if (department == null) throw new NullPointerException("Data department not found");

        Employee employee = employeeOptional.get();
        employee.setFirstName(request.getFirstName());
        employee.setLastName(request.getLastName());
        employee.setGender(request.getGender());
        employee.setHireDate(request.getHireDate());
        employee.setPlaceOfBirth(request.getPlaceOfBirth());
        employee.setDateOfBirth(request.getDateOfBirth());
        employee.setAddress(request.getAddress());

        System.out.println(employee);
        employeeRepository.save(employee);
        System.out.println(employee.getId());

        DepartmentEmployee departmentEmployee = departmentEmployeeRepository.findByEmployeeId(employee);
        if (departmentEmployee == null) {
            departmentEmployee = new DepartmentEmployee();
            departmentEmployee.setEmployeeId(employee);
        }

        departmentEmployee.setDepartmentId(department);
        departmentEmployeeRepository.save(departmentEmployee);

        return GeneralResponse.builder().data(request).status(HttpStatus.OK).message("Success update").build();
    }

    @Transactional(rollbackFor = { SQLException.class, NullPointerException.class })
    public GeneralResponse delete(Integer id) {
        Optional<Employee> employeeOptional = employeeRepository.findById(id);
        if (employeeOptional.isEmpty()) throw new NullPointerException("Data employee not found");

        employeeRepository.deleteById(id);
        return GeneralResponse.builder().data(employeeOptional.get()).status(HttpStatus.OK).message("Success delete").build();
    }

    public GeneralResponse readData(Integer id) {

        Optional<Employee> employeeOptional = employeeRepository.findById(id);
        return GeneralResponse.builder().data(employeeOptional.isEmpty() ?  null : employeeOptional.get()).status(HttpStatus.OK).message("Success get data").build();
    }

    public Department getDepartment(Integer departmentId) {
        Optional<Department> department = departmentRepository.findById(departmentId);
        return department.get();
    }
}
