package com.ist.employeeservice.controller;

import com.ist.employeeservice.dto.EmployeeRequest;
import com.ist.employeeservice.dto.EmployeeUpdateRequest;
import com.ist.employeeservice.dto.GeneralResponse;
import com.ist.employeeservice.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping(value = "/employee")
public class EmployeeController {

    @Autowired
    EmployeeService employeeService;

    @ResponseBody
    @GetMapping(value = "/all")
    public GeneralResponse allEmployee() {
        return employeeService.getAllEmployee();
    }

    @ResponseBody
    @GetMapping(value = "/read/{id}")
    public GeneralResponse readEmployee(@PathVariable(value = "id",required = true) Integer id) {
        return employeeService.readData(id);
    }

    @ResponseBody
    @PostMapping(value = "/create", produces = MediaType.APPLICATION_JSON_VALUE)
    public GeneralResponse createEmployee(@Valid @RequestBody EmployeeRequest request) {
        return employeeService.createEmployee(request);
    }

    @ResponseBody
    @PutMapping(value = "/update/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public GeneralResponse updateEmployee(@Valid @RequestBody EmployeeUpdateRequest request, @PathVariable(value = "id", required = true) Integer id) {
        return employeeService.update(request, id);
    }

    @ResponseBody
    @DeleteMapping(value = "/delete/{id}")
    public GeneralResponse deleteEmployee(@PathVariable(value = "id", required = true) Integer id) {
        return employeeService.delete(id);
    }
}
