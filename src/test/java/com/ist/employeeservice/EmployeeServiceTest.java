package com.ist.employeeservice;

import com.ist.employeeservice.controller.EmployeeController;
import com.ist.employeeservice.dto.EmployeeRequest;
import com.ist.employeeservice.dto.EmployeeUpdateRequest;
import com.ist.employeeservice.dto.GeneralResponse;
import com.ist.employeeservice.model.Employee;
import com.ist.employeeservice.service.EmployeeService;
import org.hibernate.exception.DataException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import java.util.Arrays;

@SpringBootTest
@ExtendWith(MockitoExtension.class)
public class EmployeeServiceTest {

    @Autowired
    EmployeeService employeeService;

    @InjectMocks
    EmployeeController employeeController;

    @Test
    void getAllEmployee_shouldNotNull() {
        GeneralResponse response = employeeService.getAllEmployee();
        Assertions.assertNotNull(response.getMessage());
        Assertions.assertNotNull(response.getStatus());
    }

    @Test
    void createEmployeeRequest_shouldNotNull() {
        EmployeeRequest employeeRequest = new EmployeeRequest();
        Assertions.assertNotNull(employeeRequest);
    }

    @Test
    void deleteEmployee_shouldNotNull() {
        try {
            GeneralResponse generalResponse = employeeService.delete(1);
            Assertions.assertNotNull(generalResponse);
        } catch (NullPointerException exception) {
            Assertions.assertEquals("Data employee not found", exception.getMessage());
        }
    }

    @Test
    void updateRequestSetterGetter_shouldEquals() {
        EmployeeUpdateRequest request = new EmployeeUpdateRequest();
        request.setGender("MALE");
        Assertions.assertEquals("MALE", request.getGender());
    }

    @Test
    void createEmployee_shouldException() {
        try {
            EmployeeRequest request = new EmployeeRequest();
            employeeService.createEmployee(request);
        } catch (InvalidDataAccessApiUsageException exception) {
            Assertions.assertTrue(exception.getMessage().contains("must not be null"));
        }
    }

    @Test
    void createEmployee_shouldExceptionExistData() {
        try {
            EmployeeRequest request = new EmployeeRequest();
            request.setNik("1673032805970001");
            request.setAddress("Jl. asdasd");
            request.setGender("M");
            request.setFirstName("Muhamad");
            request.setLastName("Sasas");
            request.setPlaceOfBirth("Location");
            request.setDepartmentId(1);
            employeeService.createEmployee(request);
        } catch (DataException exception) {
            Assertions.assertTrue(exception.getMessage().contains("1673032805970001"));
        }
    }

    @Test
    void testGetAllEmployee() {
        Employee employees = new Employee();
        employees.setId(1);
        employees.setNik("1673032805970001");
        employees.setAddress("Jl. asdasd");
        employees.setGender("M");
        employees.setFirstName("Muhamad");
        employees.setLastName("Sasas");
        employees.setPlaceOfBirth("Location");
        employees.setAddress("kadsfsdfsdf");

        Mockito.when(employeeService.readData(1)).thenReturn(GeneralResponse.builder().data(employees).build());

        GeneralResponse result = employeeController.readEmployee(1);
        Assertions.assertSame(result.getData(), employees);

        Employee res = (Employee)result.getData();
        Assertions.assertEquals(res.getFirstName(), employees.getFirstName());
    }
}
