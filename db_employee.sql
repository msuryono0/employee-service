-- --------------------------------------------------------
-- Host:                         localhost
-- Server version:               8.0.28 - MySQL Community Server - GPL
-- Server OS:                    Win64
-- HeidiSQL Version:             11.2.0.6213
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Dumping database structure for db_employee
CREATE DATABASE IF NOT EXISTS `db_employee` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `db_employee`;

-- Dumping structure for table db_employee.departments
CREATE TABLE IF NOT EXISTS `departments` (
  `id` int NOT NULL AUTO_INCREMENT,
  `dept_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Dumping data for table db_employee.departments: ~0 rows (approximately)
DELETE FROM `departments`;
/*!40000 ALTER TABLE `departments` DISABLE KEYS */;
INSERT INTO `departments` (`id`, `dept_name`) VALUES
	(1, 'IT'),
	(2, 'OB');
/*!40000 ALTER TABLE `departments` ENABLE KEYS */;

-- Dumping structure for table db_employee.department_employees
CREATE TABLE IF NOT EXISTS `department_employees` (
  `id` int NOT NULL AUTO_INCREMENT,
  `department_id` int DEFAULT NULL,
  `employee_id` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKryt2cfsmydbryx02tdg8ipb0c` (`department_id`),
  KEY `FKdi4yhnpwlb593bk96l4w5sbf` (`employee_id`),
  CONSTRAINT `FKdi4yhnpwlb593bk96l4w5sbf` FOREIGN KEY (`employee_id`) REFERENCES `employees` (`id`),
  CONSTRAINT `FKryt2cfsmydbryx02tdg8ipb0c` FOREIGN KEY (`department_id`) REFERENCES `departments` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Dumping data for table db_employee.department_employees: ~0 rows (approximately)
DELETE FROM `department_employees`;
/*!40000 ALTER TABLE `department_employees` DISABLE KEYS */;
INSERT INTO `department_employees` (`id`, `department_id`, `employee_id`) VALUES
	(1, 1, 1),
	(2, 1, 2);
/*!40000 ALTER TABLE `department_employees` ENABLE KEYS */;

-- Dumping structure for table db_employee.employees
CREATE TABLE IF NOT EXISTS `employees` (
  `id` int NOT NULL AUTO_INCREMENT,
  `address` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `date_of_birth` datetime DEFAULT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `gender` varchar(255) DEFAULT NULL,
  `hire_date` datetime DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `nik` varchar(16) DEFAULT NULL,
  `place_of_birth` varchar(255) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted` bit(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Dumping data for table db_employee.employees: ~0 rows (approximately)
DELETE FROM `employees`;
/*!40000 ALTER TABLE `employees` DISABLE KEYS */;
INSERT INTO `employees` (`id`, `address`, `created_at`, `date_of_birth`, `first_name`, `gender`, `hire_date`, `last_name`, `nik`, `place_of_birth`, `updated_at`, `deleted`) VALUES
	(1, 'JL.nuisdhdnfds f dsfdsf', NULL, '1997-01-08 07:08:00', 'Muhammad', 'M', '2022-01-09 07:09:00', 'Suryono', '1673032805970001', 'LLG', NULL, b'1'),
	(2, 'JL.nuisdhdnfds f dsfdsf', NULL, '1997-01-08 07:08:00', 'Muhammad', 'M', '2022-01-09 07:09:00', 'Suryono', '1673032805970002', 'LLG', NULL, b'0');
/*!40000 ALTER TABLE `employees` ENABLE KEYS */;

-- Dumping structure for table db_employee.hibernate_sequence
CREATE TABLE IF NOT EXISTS `hibernate_sequence` (
  `next_val` bigint DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Dumping data for table db_employee.hibernate_sequence: ~1 rows (approximately)
DELETE FROM `hibernate_sequence`;
/*!40000 ALTER TABLE `hibernate_sequence` DISABLE KEYS */;
INSERT INTO `hibernate_sequence` (`next_val`) VALUES
	(1);
/*!40000 ALTER TABLE `hibernate_sequence` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
